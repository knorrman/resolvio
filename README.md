# Resolvio
The goal of the project is to produce a simple resolution based theorem prover.

# Usage
The solver is developed and tested using Linux and Python 3, but may work in
other configurations as well.

The solver is run from the commandline.  It accepts a propositional logic
formula in Conjunctive Normal Form (CNF) as commandline argument; it returns 
1 if the formula is provable and 0 otherwise.

The propositional CNF formula must be specified as a set of disjunctive clauses 
in Negational Normal Form.  A (disjunctive) clause is represented by a set of
literals.  Clauses are written with explicit parenthesis and are separated by
a single ampersand symbol `&`.  A literal is expressed by the symbol `x`
followed by an integer index.  Literals may be negated, in which case they 
are prepended by the symbol `-`.

To avoid that the shell interprets the clauses as several distinct commandline
arguments, the set of clauses must be enclosed in quotes.

Example:

The formula (x0 ∨ x1 ∨ x2) ∧ (x1 ∨ x3) ∧ (x2 ∨ ¬x3) is checked using the
following command
```
# py-resolvio -cnf "(x0, x1, x3)&(x1, x3)&(x2, -x3)"
Resolving: [[(True, 0), (True, 1), (True, 3)], [(True, 1), (True, 3)], [(True, 2), (False, 3)]]
True
```
The last line prints `True` if the given formula is satisfiable and `False`
otherwise.

# Installation
To install the package, clone the repository, run the following commands:
```
# make release
# pip install .
```

# Testing
Testing is done using the pytest framework.  All tests can be run from the 
top directory by issuing the command `make test`.

# Commit Policy
Each commit shall contain a single idea, i.e., shall be atomic.

The corresponding commit message shall begin with a less than 80 character
single line description, which shall start with a type tag.  The tag is one of
the following:
 - [add] for an added feature (including test cases and scaffolding)
 - [fix] for fixing a bug or modifying an existing feature
 - [tst] for adding tests or test scaffolding
 - [doc] for added or modified documentation and comments
 - [refact] for refactoring
 - [style] for spelling corrections, white space clean-ups, PEP8 etc

After the initial line, the commit message may contain a blank line followed
by more details and clarifications.

# Branching Policy
For new larger features a feature branch shall be created.  Once the feature is
completed, the branch shall be locally merged into the master branch before
a commit to the master branch is made.  Smaller commits can be made directly
to the master branch.  Since the project only has one committer, this simple
strategy will be sufficient.

