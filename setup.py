from setuptools import setup, find_packages

setup(
        name= "resolvio",
        version = "0.9",
        description = "Simple therorem prover",
        author = "Karl Norrman",
        author_email = "karl@example.com",
        install_requires = ["pyparsing"],
        packages = find_packages(),
          entry_points = {
            "console_scripts": [
                "py-resolvio = resolvio.resolvio:main",
            ]
        }
)
