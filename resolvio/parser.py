from pyparsing import *
from pyparsing import pyparsing_common as ppc

# The function takes a string as input and returns a representation of
# the string as a propositional formula in Conjunctional Normal Form if
# the string is of the correct form and raise an exception otherwise.
#
# The clauses are enclosed in parenthesis and separated by '|'.  The
# literals consists of the symbol 'x' immediately followed by an integer.
# They may also be negated.  This is achived by prefixing them by the
# symbol '-'.  Literals are separated by a comma.
#
# - Literals are represented by tuples (bool, int), where the bool
#   represent whether the literal is negated or not and int is the index
#   of the atom.
#
# - Disjunctive clauses are represented by lists of literals.
#
# - a CNF fomrmula is represented by a list of disjunctive clauses
def parseCNF(s):
    def toADTVariable(x):
        return (False, x[2]) if x[0] == '-' else (True, x[1])

    def toADTClause(x):
        return list(map(toADTVariable, x))

    def toADTCNF(l):
        return list(map(toADTClause, l))

    LP, RP = map(Suppress, "()")
    sign = ZeroOrMore("-")
    variable = Group(Optional(sign) + Word("x") + ppc.number())
    variableList = delimitedList(variable, delim=",")

    clause = LP + Group(variableList) + RP
    clauseList = delimitedList(clause, delim="&")

    cnfParser = clauseList + lineEnd
    concreteParsedString = cnfParser.parseString(s)

    return toADTCNF(concreteParsedString)

