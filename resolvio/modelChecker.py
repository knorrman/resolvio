from itertools import product
from resolvio.resolvio import *

class ModelChecker():
    # Exhuastively try all valuations and determine whether they satisfy
    # the CNF.  If a single one is found the CNF is satisfiable by definition.
    #
    # The method returns true if the CNF is satisfiable and false otherwise
    def isSatisfiable(self, clauses):
        if clauses == []:
            return True
        if [] in clauses:
            return False

        props = allPropositions(clauses)
        truthValGenerator = product([True, False], repeat = len(props))

        for truthValues in truthValGenerator:
            valuation = dict(zip(props, truthValues))
            if evalCNF(clauses, valuation):
                return True

        return False
