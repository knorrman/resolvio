#!/usr/bin/env python
import argparse
from itertools import product, groupby
from functools import reduce

from resolvio.parser import *


# Return the list of clauses where duplicates are removed
def uniqueClauses(clauses):
    def litCmpKey(x):
        return x[1]

    def clauseCmpKey(x):
        return x[0]

    s = sorted([sorted(x, key=litCmpKey) for x in clauses], key=clauseCmpKey)
    sUnique = [x for x, _ in groupby(s)]
    return sUnique



def complement(literal):
    return (not literal[0], literal[1])


# Predictate for whether a literal and its negation are both present in a clause
def isTrivial(clause):
    for literal in clause:
        if complement(literal) in clause:
            return True
    return False


def removeTrivial(clauses):
    return list(filter((lambda x: not isTrivial(x)), clauses))


# Returns all unique propositions in the given clauses.
# A proposition is a literal without its polarity
def allPropositions(clauses):
    props = [x[1] for x in allLiterals(clauses)]
    uniqueProps = [i for i, _ in groupby(sorted(props))]
    return uniqueProps


# Returns a list of literals with all duplicates removed
def allLiterals(clauses):
    if clauses == []:
        return []
    literals = reduce((lambda lst, literal: lst + literal), clauses)
    uniqueLiterals = [i for i, _ in groupby(sorted(literals))]
    return uniqueLiterals


#
# Satisfiability via the DPLL algorithm
#
def isSatisfiable(clauses):
    def unitPropagation(unitClause, clauseSet):
        literal = unitClause[0]
        return list(filter((lambda x: not literal in x), clauseSet))

    def unitResolution(unitClause, clauseSet):
        compLiteral = complement(unitClause[0])
        for clause in clauseSet:
            if compLiteral in clause:
                clause.remove(compLiteral)
        return clauseSet

    s = uniqueClauses(clauses)
    while True:
        sLoopStart = s.copy()
        sPrime = filter((lambda c: len(c) == 1), s)
        for clause in sPrime:
            s = unitPropagation(clause, s)
            s = unitResolution(clause, s)

        # The empty set of clauses corresponds to true
        if s == []:
            return True

        # If we derived an empty clause however, it means the
        # the CNF is false (it contains a clause that can never be satisfied
        if [] in s:
            return False

        if s == sLoopStart:
            break

    # DPLL uses a splitting rule instead of the resolution rule used
    # by the DP algorithm
    aLiteral = s[0][0]     # We can select any literal and s is not empty
    sPrime = s.copy()
    sPrime.append([aLiteral])
    if (isSatisfiable(sPrime)):
        return True
    else:
        s.append([complement(aLiteral)])
        if isSatisfiable(s):
            return True
        else:
            return False


# A literal is true in two cases:
#   Either proposition is true and there is no negation  or
#   the propsition is false, but there is a negation
# val is a map from propositions (integers) to truth values of the proposition
def evalLiteral(literal, val):
    return literal[0] == val[literal[1]]


# val is a map from propositions to truth values of the proposition
def evalClause(clause, val):
    return reduce((lambda acc, x: acc or evalLiteral(x, val)), clause, False)


# val is a map from propositions to truth values of the proposition
def evalCNF(clauses, val):
    return reduce((lambda acc, x: acc and evalClause(x, val)), clauses, True)


def main():
    ap = argparse.ArgumentParser("resolvio")
    ap.add_argument("-cnf", "--cnf", action="store", type=str,\
                    help="determine whether the CNF is satisfiable")
    args = ap.parse_args()
    cnf = parseCNF(args.cnf)

    print("Resolving: {}".format(cnf))
    print(isSatisfiable(cnf))


if __name__ == "__main__":
    main()
