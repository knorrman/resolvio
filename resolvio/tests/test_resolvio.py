import random
from resolvio.resolvio import *

# Literals x0, x1, x2, ... and their complements
xs = [(True, i) for i in range(10)]
xcs = [(False, x[1]) for x in xs]


# Return true for enabling fuzz tests.  These take take some time to run
# so may be useful to disable them during development and refactoring bursts.
def runFuzzTests():
    return False

# When comparing to indiviual clauses, each one has to be encapsulated in a list
def clausesEq(l1, l2):
    def litKey(x):
        return x[1]

    def clauseKey(x):
        return x[0]

    if (l1 == [] and l2 == []) or (l1 == [[]] and l2 == [[]]):
        return True

    s1 = sorted([sorted(x, key=litKey) for x in l1], key=clauseKey)
    s2 = sorted([sorted(x, key=litKey) for x in l2], key=clauseKey)

    print("clauseEq:\n-- s1: {}\n-- s2: {}".format(s1, s2))
    return s1 == s2


def litClausesEq(l1, l2):
    return l1[0] == l2[0] and clausesEq(l1[1:], l2[1:])


# We generate random CNFs and compare the results of the model checker with
# the resolver.  If there is a difference, we log the offending CNF for
# debugging.  Becuase of this we have no strong need to have a repeatable
# sequence of CNFs.  It can be viewed as model-based fuzz-testing.
def generateLiterals(nLiterals):
    i = 0
    while i < nLiterals:
        neg = random.choice((False, True))
        yield (neg, i)
        i = i + 1


def generateClauses(nClauses, maxLiterals):
    i = 0
    while i < nClauses:
        yield [lit for lit in generateLiterals(random.randint(1, maxLiterals))]
        i = i + 1


def generateCNF(maxLiterals, maxClauses):
    nClauses = random.randint(1, maxClauses)
    return [c for c in generateClauses(nClauses, maxLiterals)]


# Explicit function tests with hand-coded test oracles for the various functions
# of the resolution prover
class TestResolution():

    # Intention test the test harness. Test whether it returns true for two
    # equal sets return
    def test_clausesEq0(self):
        assert clausesEq([[xcs[1], xs[4]], [xs[2]], [xs[3], xs[6], xs[5]]],
                         [[xs[2]], [xs[3], xs[5], xs[6]], [xcs[1], xs[4]]])

    # Intention: test whether it returns false for two different sets
    def test_clausesEq1(self):
        assert not clausesEq([[xs[1]], [xs[2]], [xs[3]]],
                             [[xs[4]], [xs[3]], [xs[1]]])

    # Intention: test whether it returns false for two different sets
    # when the first is the empty set
    def test_clausesEq2(self):
        assert not clausesEq([], [[xs[4]], [xs[3]], [xs[1]]])

    # Intention: test whether it returns false for two different sets
    # when the second is the empty set
    def test_clausesEq3(self):
        assert not clausesEq([[xs[1]], [xs[2]], [xs[4]]], [])

    # Intention: test whether it returns true when both inputs are the
    # empty set
    def test_clausesEq4(self):
        assert clausesEq([], [])

    # Intention: test whether it returns true when both inputs are the
    # sets containing the empty set
    def test_clausesEq5(self):
        assert clausesEq([[]], [[]])

    # ---

    # Intention: correct identification of trivial clause
    def test_isTrivial0(self):
        clause = [xs[0], xcs[2], xcs[1], xs[2]]
        assert isTrivial(clause)

    # Intention: correct identification of non-trivial clause
    def test_isTrivial1(self):
        clause = [xs[0], xcs[3], xcs[1], xs[2]]
        assert not isTrivial(clause)

    # ---

    # Intention: correct removal of trivial clauses
    def test_removeTrivial0(self):
        clauses = [
            [xs[4], xcs[2], xcs[1], xs[3]],
            [xs[2], xcs[2], xcs[1], xs[4]],   # Trivial
            [xs[3], xcs[2], xcs[1], xs[0]],
            [xcs[1], xcs[2], xcs[1], xs[2]],  # Trivial
            [xs[5], xcs[3], xs[1], xcs[5]],   # Trivial
        ]
        expectedAns = [
            [xs[4], xcs[2], xcs[1], xs[3]],
            [xs[3], xcs[2], xcs[1], xs[0]]
        ]
        assert clausesEq(removeTrivial(clauses), expectedAns)

    # ---

    # Intention: correctly finds all literals in clause set ignoring duplicates
    def test_allLiterals0(self):
        clauses = [
            [xs[0], xs[1], xs[2]],
            [xs[2], xs[3], xcs[4]]
        ]
        expectedAns = [
            xs[0], xs[1], xs[2],
            xs[3], xcs[4]
        ]
        assert sorted(allLiterals(clauses)) == sorted(expectedAns)

    # Intention: correctly finds no literals in empty clause
    def test_allLiterals1(self):
        clauses = [[]]
        expectedAns = []
        assert sorted(allLiterals(clauses)) == sorted(expectedAns)

    # Intention: correctly finds no literals in the empty formula
    def test_allLiterals2(self):
        clauses = []
        expectedAns = []
        assert sorted(allLiterals(clauses)) == sorted(expectedAns)

    # ---

    # Intention: correctly finds all propositions in clause set ignoring
    # duplicates
    def test_allPropositions0(self):
        clauses = [
            [xs[0], xs[1], xs[2]],
            [xs[2], xs[3], xcs[4], xcs[0], xcs[9]]
        ]
        expectedAns = [
            xs[0][1], xs[1][1], xs[2][1], xs[9][1],
            xs[3][1], xs[4][1]
        ]
        assert sorted(allPropositions(clauses)) == sorted(expectedAns)

    # Intention: correctly finds no propositions in the empty clause
    def test_allPropositions1(self):
        clauses = [[]]
        expectedAns = []
        assert sorted(allPropositions(clauses)) == sorted(expectedAns)

    # Intention: correctly finds no propositions in the empty formula
    def test_allPropositions1(self):
        clauses = []
        expectedAns = []
        assert sorted(allPropositions(clauses)) == sorted(expectedAns)

    # ---

    # Intention: correctly determines CNF unsatisfiability
    def test_isSatisfiable0(self):
        clauses = [
            [xs[1], xs[2]],
            [xs[1], xcs[2]],
            [xcs[1], xs[2]],
            [xcs[2], xcs[1]]
        ]
        expectedAns = False
        #assert modelCheck(clauses) == expectedAns
        assert isSatisfiable(clauses) == expectedAns

    # Intention: correctly determines CNF satisfiability
    def test_isSatisfiable1(self):
        clauses = [
            [xs[1], xs[0]],
            [xcs[2], xcs[1]]
        ]
        expectedAns = True
        assert isSatisfiable(clauses) == expectedAns


# Model Checker tests.  The model checker is then used to exhaustively determine
# whether all models satisfy the clauses.  If it does, we can conclude from
# the completeness of propositional logic that the the CNF is valid and hence
# is provable.
#
# We then generate CNFs, use the model checker to determine whether they are
# valid and then check whether the resolution prover reaches the same.
# If it does it is deemed correct for that CNF.
#
# We use this to stress-test the prover.


# A valuation is a map from propositions to booleans.  This map is used
# to simplify writing test case inputs to the model checker.
valuation = {
    xs[0][1]: True,
    xs[1][1]: True,
    xs[2][1]: True,
    xs[3][1]: True,
    xs[4][1]: False,
    xs[5][1]: False,
    xs[6][1]: False,
    xs[7][1]: False,
}


class TestEvaluation:
    # Intention: correctly evaluates a literal with a true proposition
    # and no negation
    def test_evalLiteral0(self):
        literal = xs[0]
        expectedAns = True
        assert evalLiteral(literal, valuation) == expectedAns

    # Intention: correctly evaluates a literal with a false proposition
    # and no negation
    def test_evalLiteral1(self):
        literal = xs[4]
        expectedAns = False
        assert evalLiteral(literal, valuation) == expectedAns

    # Intention: correctly evaluates a literal with a true proposition
    # and a negation
    def test_evalLiteral2(self):
        literal = xcs[0]
        expectedAns = False
        assert evalLiteral(literal, valuation) == expectedAns

    # Intention: correctly evaluates a literal with a false proposition
    # and a negation
    def test_evalLiteral3(self):
        literal = xcs[4]
        expectedAns = True
        assert evalLiteral(literal, valuation) == expectedAns

    #
    # ---
    #

    # Intention: correctly evaluates a clause with one literal
    # where the proposition is true
    def test_evalClause0(self):
        clause = [xs[0]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with one literal
    # where the proposition is false but negated
    def test_evalClause1(self):
        clause = [xcs[4]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and the first one is the only true proposition (boundary case)
    def test_evalClause2(self):
        clause = [xs[0], xs[4], xs[5], xs[6]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and the last one is the only true proposition (boundary case)
    def test_evalClause3(self):
        clause = [xs[4], xs[5], xs[6], xs[0]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and the first one is the only false but negated proposition
    # (boundary case)
    def test_evalClause4(self):
        clause = [xcs[6], xs[4], xs[5], xs[6]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and the last one is the only false but negated proposition (boundary case)
    def test_evalClause5(self):
        clause = [xs[4], xs[5], xs[6], xcs[7]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and a middle proposition is the only true proposition.
    # All other propositions are false (boundary case)
    def test_evalClause6(self):
        clause = [xs[6], xs[0], xs[5], xs[6]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and a middle proposition is the only false but negated proposition.
    # All other propositions are false (boundary case)
    def test_evalClause7(self):
        clause = [xs[4], xcs[5], xs[6], xs[7]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with one literal
    # where the proposition is false
    def test_evalClause8(self):
        clause = [xs[4]]
        expectedAns = False
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with one literal
    # where the proposition is True but negated
    def test_evalClause9(self):
        clause = [xcs[0]]
        expectedAns = False
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and none is negated. All propositions are false (boundary case)
    def test_evalClause10(self):
        clause = [xs[4], xs[5], xs[6], xs[7]]
        expectedAns = False
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and all are negated. All propositions are false (boundary case)
    def test_evalClause11(self):
        clause = [xcs[4], xcs[5], xcs[6], xcs[7]]
        expectedAns = True
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates a clause with more than one literal
    # and all are negated. All propositions are true (boundary case)
    def test_evalClause12(self):
        clause = [xcs[0], xcs[1], xcs[2], xcs[3]]
        expectedAns = False
        assert evalClause(clause, valuation) == expectedAns

    # Intention: correctly evaluates the empty class to false
    # Witout any literals in the clause, there is no valution that
    # can make it true
    def test_evalClause13(self):
        clause = []
        expectedAns = False
        assert evalClause(clause, valuation) == expectedAns

    #
    # ---
    #

    # Intention: correctly evaluates a CNF that should yield true
    def test_evalCNF0(self):
        clauses = [
            [xcs[6], xs[4], xs[5], xs[6]],
            [xcs[4], xcs[5], xcs[6], xcs[7]],
            [xs[4], xs[5], xs[6], xcs[7]]
        ]
        expectedAns = True
        assert evalCNF(clauses, valuation) == expectedAns

    # Intention: correctly evaluates a CNF that should yield false
    def test_evalCNF1(self):
        clauses = [
            [xcs[6], xs[4], xs[5], xs[6]],
            [xcs[4], xcs[5], xcs[6], xcs[7]],
            [xs[4]]
        ]
        expectedAns = False
        assert evalCNF(clauses, valuation) == expectedAns

    # Intention: correctly evaluates a single cluase CNF that should yield true
    def test_evalCNF2(self):
        clauses = [[xs[0]]]
        expectedAns = True
        assert evalCNF(clauses, valuation) == expectedAns

    # Intention: correctly evaluates a single cluase CNF that should yield false
    def test_evalCNF3(self):
        clauses = [[xcs[0]]]
        expectedAns = False
        assert evalCNF(clauses, valuation) == expectedAns

    # Intention: correctly evaluates an empty CNF
    def test_evalCNF4(self):
        clauses = [[]]
        expectedAns = False
        assert evalCNF(clauses, valuation) == expectedAns

    #
    # ---
    #

    #
    # Stress/Fuzz tests.
    #
    # These take a few minutes to run, so they can be enabled/disabled
    # using the function runFuzzTests() defined above

    # Intention: compare the resolution based solver against the simple
    # exhaustive search model checker on pseudo-randomly chosen formulas
    # First case: a few complicated formulas
    # Time: Approx 30sec to 3 minutes to run
    def test_fuzz0(self):
        if runFuzzTests():
            maxLiterals = 20
            maxClauses = 20
            for i in range(5):
                clause = generateCNF(maxLiterals, maxClauses)
                expectedAns = modelCheckSatisfiable(clause)
                assert isSatisfiable(clause) == expectedAns

    # Intention: compare the resolution based solver against the simple
    # exhaustive search model checker on pseudo-randomly chosen formulas
    # Second case: many simpler formulas
    # Time: Approx 6 seconds to run
    def test_fuzz1(self):
        if runFuzzTests():
            maxLiterals = 5
            maxClauses = 5
            for i in range(10000):
                clause = generateCNF(maxLiterals, maxClauses)
                expectedAns = modelCheckSatisfiable(clause)
                assert isSatisfiable(clause) == expectedAns


    # Intention: compare the resolution based solver against the simple
    # exhaustive search model checker on pseudo-randomly chosen formulas
    # Third case: many simple clauses
    # Time: Approx 10-20 seconds to run
    def test_fuzz2(self):
        if runFuzzTests():
            maxLiterals = 5
            maxClauses = 100
            for i in range(5000):
                clause = generateCNF(maxLiterals, maxClauses)
                expectedAns = modelCheckSatisfiable(clause)
                assert isSatisfiable(clause) == expectedAns

    # Intention: compare the resolution based solver against the simple
    # exhaustive search model checker on pseudo-randomly chosen formulas
    # Fourth case: few complicated clauses
    # Time: Approx 2 seconds to run
    def test_fuzz3(self):
        if runFuzzTests():
            maxLiterals = 1000
            maxClauses = 5
            for i in range(100):
                clause = generateCNF(maxLiterals, maxClauses)
                expectedAns = modelCheckSatisfiable(clause)
                assert isSatisfiable(clause) == expectedAns
