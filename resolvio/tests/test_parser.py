import pytest
from resolvio.parser import *

# Literals x0, x1, x2, ... and their complements
xs = [(True, i) for i in range(10)]
xcs = [(False, x[1]) for x in xs]

class TestParser():
    # Intention: testing whether badly formed CNF raises error correctly
    def test_parseNonCNFRaisesException(self):
        s = "(-x0)(x1)"
        with pytest.raises(Exception):
            parseCNF(s)

    # Intention: testing whether garbage at the end raises error correctly
    def test_parseGarbageAtEndRaisesException(self):
        s = "(-x0)&(x1)garbage"
        with pytest.raises(Exception):
            parseCNF(s)

    # Intention: testing whether garbage at the start raises error correctly
    def test_parseGarbageAtStartRaisesException(self):
        s = "garbage(-x0)&(x1)"
        with pytest.raises(Exception):
            parseCNF(s)

    #
    #---
    #

    # Intention: correctly parses a clause of a single literal
    def test_parseClauseOfASingleLiteral(self):
        s = "(x0)"
        expectedAns = [[(True, 0)]]
        assert parseCNF(s) == expectedAns

    # Intention: correctly parses a clause of a multiple literals
    def test_parseClauseOfMultipleLiterals(self):
        s = "(x0, x1,-x2,  -x5 )"
        expectedAns = [[(True, 0), (True, 1), (False, 2), (False, 5)]]
        assert parseCNF(s) == expectedAns

    # Intention: correctly parses a more complex multi-clause formula
    def test_parseFormula(self):
        s = "(x0, x1,-x2, -x4)&(x0)&(-x1, x5)"
        expectedAns = [
                [(True, 0), (True, 1), (False, 2), (False, 4)],
                [(True, 0)],
                [(False, 1), (True, 5)]
                ]
        assert parseCNF(s) == expectedAns


