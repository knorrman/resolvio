from resolvio import *
from resolvio.modelChecker import *


# Literals x0, x1, x2, ... and their complements
xs = [(True, i) for i in range(10)]
xcs = [(False, x[1]) for x in xs]

class TestModelChecker():
    # Intention: correctly determines CNF is satisfiable
    # Empty list of clauses shold yield true
    def test_modelCheckSAT0(self):
        clauses = []
        expectedAns = True
        mc = ModelChecker()
        assert mc.isSatisfiable(clauses) == expectedAns

    # Intention: correctly determines CNF is NOT satisfiable
    # Single empty clause should yield false
    def test_modelCheckSAT1(self):
        clauses = [[]]
        expectedAns = False
        mc = ModelChecker()
        assert mc.isSatisfiable(clauses) == expectedAns

    # Intention: correctly determines that CNF is satisfiable
    # Single clause is satisfiable
    # This test makes sure single literal case works
    def test_modelCheckSAT2(self):
        clauses = [[xs[0]]]
        expectedAns = True
        mc = ModelChecker()
        assert mc.isSatisfiable(clauses) == expectedAns

    # Intention: correctly determines that CNF is satisfiable
    # Multiple literals in single clause
    def test_modelCheckSAT3(self):
        clauses = [
            [xs[0], xs[1], xcs[0]]
        ]
        expectedAns = True
        mc = ModelChecker()
        assert mc.isSatisfiable(clauses) == expectedAns

    # Intention: correctly determines that CNF is satisfiable
    # Multiple literals in one clause, single literal in other
    def test_modelCheckSAT4(self):
        clauses = [
            [xs[0], xs[1], xcs[0]],
            [xs[0]]
        ]
        expectedAns = True
        mc = ModelChecker()
        assert mc.isSatisfiable(clauses) == expectedAns

    # Intention: correctly refutes that a CNF is satisfiable
    # Muiltiple literals in all clauses
    def test_modelCheckSAT5(self):
        clauses = [
            [xs[0], xs[1], xcs[0]],
            [xs[0], xs[1], xcs[1]],
            [xs[3], xs[0], xcs[1]]
        ]
        expectedAns = True
        mc = ModelChecker()
        assert mc.isSatisfiable(clauses) == expectedAns

    # Intention: correctly concludes a CNF as not satisfiable
    def test_modelCheckSAT6(self):
        clauses = [
            [xs[0], xs[1], xs[2]],
            [xcs[0], xcs[1], xcs[2]],
            [xs[0], xcs[1]],
            [xcs[0], xs[1]],
            [xs[1], xcs[2]],
            [xcs[1], xs[2]]
        ]
        expectedAns = False
        mc = ModelChecker()
        assert mc.isSatisfiable(clauses) == expectedAns

