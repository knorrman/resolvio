test:
	pytest resolvio/tests

release:
	python setup.py sdist

clean:
	rm -fr resolvio.egg-info
	rm -fr __pycache__
	rm -fr reslovio/__pycache__
	rm -fr resolvio/tests/__pycache__
